﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DenseCRF
{
   public class util
    {
      public  static float[,,] readpng(String imgpath)
        {
            Bitmap image = new Bitmap(imgpath);
            int i, j;
            float[,,] GreyImage = new float[image.Width, image.Height,3];  //[Row,Column]
          
            BitmapData bitmapData1 = image.LockBits(new Rectangle(0, 0, image.Width, image.Height),
                                     ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* imagePointer1 = (byte*)bitmapData1.Scan0;

                for (i = 0; i < bitmapData1.Height; i++)
                {
                    for (j = 0; j < bitmapData1.Width; j++)
                    {
                        GreyImage[j, i,0] = (int)imagePointer1[0];//B
                        GreyImage[j, i, 1] = (int)imagePointer1[1];//G
                        GreyImage[j, i, 2] = (int)imagePointer1[2];//R
                        //      GreyImage[j, i, 2] = (int)imagePointer1[3];//A
                        //4 bytes per pixel
                        imagePointer1 += 4;
                    }//end for j
                    //4 bytes per pixel
                    imagePointer1 += bitmapData1.Stride - (bitmapData1.Width * 4);
                }//end for i
            }//end unsafe
            image.UnlockBits(bitmapData1); 
            image.Dispose();
            return GreyImage;
        }

        public static int getColor(float c, float c1, float c2)
        {
            return  (int)c + 256 * (int)c1 + 256 * 256 * (int)c2;
        }
        static double GT_PROB = 0.5;
        public static float[,,] classify(float[,,] anno, int M)
        {
             
            int nColors = 0;
            int[] colors=new int[255];
            int W = anno.GetLength(0); int H = anno.GetLength(1);
            float u_energy = (float)-Math.Log(1.0 / M);
            float n_energy = (float)-Math.Log((1.0f - GT_PROB) / (M - 1));
            float p_energy = (float)-Math.Log(GT_PROB);
            float[,,] res = new float[W, H, M];
            for (int k = 0; k < W; k++)
            {
                for (int j = 0; j < H; j++)
                {

                    int c = getColor(anno[k, j, 2], anno[k, j, 1], anno[k, j, 0]);
                    int i = 0;
                    for (i = 0; i < nColors && c != colors[i]; i++) ;

                    if (c != colors[i] && i == nColors)
                    {
                        if (i < M)

                            colors[nColors++] = c;
                        else
                        {
                            int min = int.MaxValue;
                            int index = -1;
                            for (int b = 0; b < nColors; b++) {
                                if (Math.Abs(colors[b] - c) < min)
                                {
                                    min = Math.Abs(colors[b] - c);
                                    index = b;
                                }
                            }
                            c = colors[index];
                            i = index;
                            //  c = 0;
                        }
                    }
                    if (c != 0)
                    {
                        for (int s = 0; s < M; s++)
                            res[k, j, s] = n_energy;
                        res[k, j, i] = p_energy;
                    }
                    else
                    {
                        for (int s = 0; s < M; s++)
                            res[k, j, s] = u_energy;
                    }
                }
            }
            //for(int k=0; k<W*H; k++ ){
            //	// Map the color to a label
            //	int c = getColor(im + 3 * k);
            //       int i;
            //	for(i=0;i<nColors && c!=colors[i]; i++ );
            //	if (c && i==nColors){
            //		if (i<M)

            //               colors[nColors++] = c;
            //		else
            //			c=0;
            //	}

            //   // Set the energy
            //   float* r = res + k * M;
            //	if (c){
            //		for(int j=0; j<M; j++ )
            //			r[j] = n_energy;
            //		r[i] = p_energy;
            //	}
            //	else{
            //		for(int j=0; j<M; j++ )
            //			r[j] = u_energy;
            //	}
            //}
            return res;

        }
           
    }
}
